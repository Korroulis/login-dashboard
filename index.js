const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const router = express.Router();
const members = require('./Members');


const app = express();
app.use(bodyParser.urlencoded({ extended: true}));

app.get('/', (req,res)=> {
    res.send('Hello');
});

const PORT = process.env.PORT || 8000;

app.get('/login', (req,res)=>{
    return res.sendFile( path.join( __dirname, 'login.html' ) );
})

app.post('/login', (req,res)=>{
    if(req.body.contactName == "Mihalis Korres" && req.body.password == "qwert") {
        return res.sendFile( path.join( __dirname, 'dashboard.html' ) );
    }
    else {
        res.send(`Invalid login ${req.body.contactName}, ${req.body.password}`);
    }
    
})

app.post('/dashboard', (req,res)=>{
    const year=parseInt(req.body.date1.split("-")[0]);
    const month=parseInt(req.body.date1.split("-")[1]);
    const day=parseInt(req.body.date1.split("-")[2]);
    const today = new Date();
    const dd = parseInt(String(today.getDate()).padStart(2, '0'));
    const mm = parseInt(String(today.getMonth() + 1).padStart(2, '0')); 
    const yyyy = today.getFullYear();

    if (year >= yyyy && month >= mm && day >= dd && $.inArray(req.body.date1, members.arr.map(item => item.date))===-1) {
        const newMember = {
            name: req.body.contactName,
            date: req.body.date1
        };
    
        members.push(newMember);
        res.send(`${req.body.date1}`); 
    }
    else if (year >= yyyy && month >= mm && day >= dd && $.inArray(req.body.date1, members.arr.map(item => item.date))!==-1) {
        //res.send(`Pick a non occupied date`); 
        return res.sendFile( path.join( __dirname, 'dashboard.html' ) );
    }

    else {  
        //res.send(`Pick a date later than current date`);       
        return res.sendFile( path.join( __dirname, 'dashboard.html' ) );
    }
})

app.get('/members', (req,res) => res.send(members));

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));